import math as ma
import matplotlib.pyplot as plt
import numpy as np
import os


def lorentz_transform(point, velocity):
    old_time = point[0]
    old_location = point[1]
    
    lorentz_factor = 1.0/(1.0 - velocity**2.0)**0.5
    new_time = lorentz_factor*(old_time - velocity*old_location)
    new_location = lorentz_factor*(old_location - velocity*old_time)
    
    return (new_location, new_time)


def make_plot(velocity, frame_num):
    # Get the sample points. 
    num_points = 160
    sample_points = []
    for i in range(num_points):
        angle = i*(2.0*ma.pi/num_points)
        sample_point = [ma.cos(angle), ma.sin(angle)]
        sample_points.append(sample_point)
    # Take the Lorentz transform.   
    transformed_points = [lorentz_transform(point, velocity) for point in sample_points]    
    # Plot the points. 
    for point in sample_points:
        plt.plot(*point, marker=".", color="blue")
    for point in transformed_points:
        plt.plot(*point, marker=".", color="red")
    # Set up the rest of the plot. 
    plt.title(f"Velocity: {velocity :.2}⋅c")
    plt.xlabel("space")
    plt.ylabel("time")
    plt.xlim(-2., 2.)
    plt.ylim(-2., 2.)
    plt.plot(0.0, 0.0, marker="o", color="black")
    # Save the plot and return. 
    plt.savefig(f"frames/frame{frame_num :06}.png")
    plt.clf()
    return None


if __name__ == "__main__":
    os.system("mkdir frames 2> /dev/null")
    velocities = np.linspace(0., .9, 100)
    velocities = np.concatenate((velocities, np.flip(velocities),
                                -velocities, -np.flip(velocities)))
    for i, velocity in enumerate(velocities):
        make_plot(velocity, i)
